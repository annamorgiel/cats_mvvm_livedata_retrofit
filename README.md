# Cats_MVVM_LiveData_Retrofit

Sample app to exercise the best practices with the Jetpack Architecture components: ViewModel and LiveData

|Package   | Type       | Name          | Description                                                                                                       |
|:--------:|:----------:|:-------------:|:-----------------------------------------------------------------------------------------------------------------:|
|Model     | interface  | CatApiService | @Query catPicturesList                                                                                            |
|Model     | data class | CatPicture    | @SerializedName id, url                                                                                           |
|Model     | class      | CatRepository | getCatPicturesList  creates and returns MutableLiveData<List<CatPicture>> with load body onResponse successful    |
|ViewModel | class      | CatViewModel  | val catListObservable: LiveData<List<CatPicture>>, catListObservable = catRepository.getCatPicturesList(10)       |
|View      | class      | CatAdapter    | compare contents CatPicture, create ViewHolder, load picture to the holder                                        |
|View      | class      | MainActivity  | create GridLayoutManager and Adapter instance, get model from the ViewModelProvider, set observable               |
|root      | class      | App           | build retrofit, create lateinit var catRepository:CatRepository                                                   | 

MainActivity contains a Grid of cat photos from one of the most important API's on the web, the https://thecatapi.com/
We set the layout manager and the adapter, fetch the instance of the CatViewModel from the ViewModelProvider and set the LifecycleObserver on the LiveData. 
When the data or its content changes, the view should redraw itself, so we pass the new CatPictures list to the adapter with the default method .submitList(cats).

CatViewModel holds the LiveData instance. The model reaches out to the CatRepository to fetch the getCatPicturesList that creates the live data,
executes the request with the catApiService and depending onResponse or onFailure passes either the content or an empty list. 
CatRepository also creates the CatApiService instance with the Retrofit library.

CatApiService creates a request providing the api-key and the amount of photos to be fetched.

App extending the Application was implemented with to provide the retrofit model and initialize the CatRepository.

In the contrary to MVP, MVVM doesn't hold a reference to a view - it has a LifecycleOwner that provides data for LiveData's LifecycleObserver.

The next feature to implement would be the detailed item screen after onClick.


